/* eslint-disable import/no-extraneous-dependencies */
import '@storybook/addon-knobs/register'
import '@storybook/addon-actions/register'
import 'storybook-readme/register'
import '@storybook/addon-viewport/register'
import '@storybook/addon-jest/register'
