import { create } from '@storybook/theming/create'

export default create({
  base: 'light',

  colorPrimary: '#7048E8',
  colorSecondary: '#7048E8',

  // UI
  appBg: '#F8F9FA',
  appContentBg: 'white',
  appBorderColor: 'rgba(0, 0, 0, 0.05)',
  appBorderRadius: 4,

  // Typography
  fontBase: '"Roboto", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: '#495057',
  textInverseColor: '#F8F9FA',

  // Toolbar default and active colors
  barTextColor: '#495057',
  barSelectedColor: '#7048E8',
  barBg: 'white',

  // Form colors
  inputBg: 'white',
  inputBorder: 'silver',
  inputTextColor: '#495057',
  inputBorderRadius: 4,

  brandTitle: 'Wivi UI',
  brandUrl: 'https://wivi.io/',
  brandImage: 'https://gitlab.com/twelve-solutions/wivi/wivi-ui/-/raw/master/logo.svg'
})
