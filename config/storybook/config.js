import { addDecorator, addParameters, configure } from '@storybook/vue'
import { addReadme } from 'storybook-readme/vue'
// import { withTests } from '@storybook/addon-jest'

// import testResults from '../.jest-test-results.json'
// import '../src/styles/index.scss'
import theme from './theme'
import viewportsList from './viewportsList'
import stories from '@/stories'

addParameters({
  viewport: {
    viewports: viewportsList,
    defaultViewport: 'iphone678'
  },
  options: {
    showPanel: true,
    panelPosition: 'right',
    theme: theme
  },
  readme: {
    // You can set the global code theme here.
    codeTheme: 'github'
  }
})

addDecorator(addReadme)
// addDecorator(withTests({ results: testResults }))

const loaderFn = () => stories
configure(loaderFn, module)
