const path = require('path')

module.exports = ({ config }) => {

  config.module.rules.push({
    test: /\.(ts|tsx)?$/,
    exclude: /node_modules/,
    include: [/src/],
    loader: 'ts-loader',
    options: {
      appendTsSuffixTo: [/\.vue$/],
      transpileOnly: true
    }
  })

  config.module.rules.push({
    test: /\.scss$/,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          localsConvention: 'camelCaseOnly',
          modules: {
            localIdentName: '[hash:base64:6]'
          }

        }
      },
      'postcss-loader',
      'sass-loader'
    ],
    include: path.resolve(__dirname, '../../')
  })

  config.module.rules.push({
    resourceQuery: /blockType=docs/,
    use: ['storybook-readme/vue/docs-loader', 'html-loader', 'markdown-loader']
  })

  config.module.rules.push({
    test: /\.md$/,
    exclude: /node_modules/,
    loader: 'markdown-loader'
  })

  config.resolve.extensions.push('.vue', '.js', '.jsx', '.ts', '.tsx', '.md')

  config.resolve.alias['~'] = path.resolve(__dirname, '../..')
  config.resolve.alias['@'] = path.resolve(__dirname, '../../src')

  return config
}
