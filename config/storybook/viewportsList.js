export default {
  iphone678: {
    name: 'iPhone 6/7/8',
    type: 'mobile',
    styles: {
      width: '375px',
      height: '667px'
    }
  },
  iphone678Plus: {
    name: 'iPhone 6/7/8 Plus',
    type: 'mobile',
    styles: {
      width: '414px',
      height: '736px'
    }
  },
  iphoneX: {
    name: 'iPhone X',
    type: 'mobile',
    styles: {
      width: '375px',
      height: '812px'
    }
  },
  galaxyS5: {
    name: 'Galaxy S5',
    type: 'mobile',
    styles: {
      width: '360px',
      height: '640px'
    }
  },
  iPad: {
    name: 'iPad',
    type: 'tablet',
    styles: {
      width: '1024px',
      height: '768px'
    }
  },
  iPadPro: {
    name: 'iPad Pro',
    type: 'tablet',
    styles: {
      width: '1366px',
      height: '1024px'
    }
  },
  macBook13: {
    name: 'MacBook 13',
    type: 'desktop',
    styles: {
      width: '1280px',
      height: '800px'
    }
  },
  macBook16: {
    name: 'MacBook 16',
    type: 'desktop',
    styles: {
      width: '1536px',
      height: '960px'
    }
  },
  imac27: {
    name: 'iMac 27',
    type: 'desktop',
    styles: {
      width: '2560px',
      height: '1440px'
    }
  }
}
