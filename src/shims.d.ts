declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module '*.scss' {
  const content: any
  export default content
}

declare module '*.md' {
  const value: string
  export default value
}
