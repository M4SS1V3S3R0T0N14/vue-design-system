import _Vue from 'vue'
import type { PluginFunction, PluginObject, VueConstructor } from 'vue'

export const use = (plugin: PluginObject<any> | PluginFunction<any>) => {
  // To auto-install on non-es builds, when vue is found
  // @ts-ignore
  if (process.env.ES_BUILD === 'false') {
    if (typeof window !== 'undefined' && window.Vue) {
      window.Vue.use(plugin)
    }
  }
}

export const registerComponent = (Vue: typeof _Vue, component: VueConstructor) => {
  Vue.component(component.name, component)
}
