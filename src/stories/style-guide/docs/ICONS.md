# Icônes

Les icônes sont un élément crucial de tout système de conception ou expérience produit. Les icônes nous aident à naviguer rapidement. Ils sont indépendants de la langue. Et le meilleur de tous: ils sont vraiment minuscules, donc ils n'occupent pas beaucoup de biens immobiliers. Les icônes sont un élément fondamental d'un bon système de conception et sont très utiles pour les supports marketing. Ils sont la pierre angulaire du contenu illustré, mais ils sont également très techniques.
