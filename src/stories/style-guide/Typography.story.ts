import { Category } from '../storiesHierarchy'
import TYPOGRAPHY from './docs/TYPOGRAPHY.md'

export default {
  title: Category.TYPOGRAPHY,
  parameters: {
    viewport: false,
    options: { showAddonPanel: false },
    readme: { content: TYPOGRAPHY }
  }
}

export const Typographie = () => ({})
