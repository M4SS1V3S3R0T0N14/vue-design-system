import { Category } from '../../storiesHierarchy'
import { WButton } from '../../../components/button'
import README from '../../../components/button/README.md'

export default {
  title: `${Category.COMPONENTS}/Button`,
  parameters: {
    readme: { sidebar: README },
    jest: ['Button.spec.ts']
  }
}

export const Button = () => ({
  components: { WButton },
  data () {
    return {
      success: false,
      loading: false
    }
  },
  template: `
  <div class="story" style="display: flex; align-items: center; justify-content: center;">
     <w-button>Button</w-button>
  </div>
  `
})
