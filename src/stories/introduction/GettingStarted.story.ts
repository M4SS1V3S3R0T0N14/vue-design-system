import { Category } from '../storiesHierarchy'
import GETTING_STARTED from '../../../README.md'

export default {
  title: Category.GETTING_STARTED,
  parameters: {
    viewport: false,
    options: { showAddonPanel: false },
    readme: { content: GETTING_STARTED }
  }
}

export const PourCommencer = () => ({
  name: 'Pour commencer'
})
