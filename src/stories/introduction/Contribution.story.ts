import { Category } from '../storiesHierarchy'
import CONTRIBUTION from '../../../CONTRIBUTING.md'

export default {
  title: Category.CONTRIBUTION,
  parameters: {
    viewport: false,
    options: { showAddonPanel: false },
    readme: { content: CONTRIBUTION }
  }
}

export const Contribuer = () => ({})
