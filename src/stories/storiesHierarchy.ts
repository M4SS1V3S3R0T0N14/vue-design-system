export const Category = {
  // Introduction
  GETTING_STARTED: 'Introduction|Pour commencer',
  CONTRIBUTION: 'Introduction|Contribuer',

  // Style Guide
  TYPOGRAPHY: 'Guide des styles|Typographie',
  GRID_AND_SPACES: 'Guide des styles|Grilles et espacements',
  COLORS: 'Guide des styles|Couleurs',
  ICONS: 'Guide des styles|Icônes',

  // Component library
  COMPONENTS: 'Bibliothèque de composants|Composants',

  // Pattern library
  PATTERNS: 'Bibliothèque de modèles|Modèles'
}
