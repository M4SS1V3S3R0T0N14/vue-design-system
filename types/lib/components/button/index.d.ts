import type { VueConstructor, PluginObject } from 'vue'

declare const Button: VueConstructor
declare const Plugin: PluginObject<any>

export default Plugin
export { Button as WButton }
