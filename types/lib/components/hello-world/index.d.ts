import type { VueConstructor, PluginObject } from 'vue'

declare const HelloWorld: VueConstructor
declare const Plugin: PluginObject<any>

export default Plugin
export { HelloWorld as WHelloWorld }
