module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  roots: ['./src'],
  testMatch: ['**/*.spec.[jt]s?(x)'],
  collectCoverage: true,
  collectCoverageFrom: [
    'src/components/**/*.{ts,vue}',
    'src/utils/**/*.{ts,vue}',
    'src/mixins/**/*.{ts, vue}'
  ]
}
